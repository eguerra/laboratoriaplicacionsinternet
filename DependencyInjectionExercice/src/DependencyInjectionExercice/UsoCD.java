package DependencyInjectionExercice;
import org.springframework.context.support.ClassPathXmlApplicationContext;
public class UsoCD {

	public static void main(String[] args) {
		
		
		ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		// 2.Pedir el Bean
		CDPlayer cdp = contexto.getBean("miCDPLayer", CDPlayer.class);
		CompactDisc cd = contexto.getBean("miCD", CD.class);
		
		cdp.setCompactDisc(cd);
		
		
		// 3.Utilizar el Bean
		cdp.play();
		
		
		contexto.close();
	}

}
