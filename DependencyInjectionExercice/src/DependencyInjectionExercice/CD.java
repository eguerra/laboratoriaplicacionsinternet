package DependencyInjectionExercice;

public class CD implements CompactDisc{
	
	private String name;
	private String artist;
	
	
	public void play() {
		
		System.out.println(name+" from "+artist);
	}
	
	
}
