package DependencyInjectionExercice;

public class CDPlayer {
	
	private CompactDisc compactDisc;

	public void setCompactDisc(CompactDisc compactDisc) {
		this.compactDisc = compactDisc;
	}
	
	public void play() {
		compactDisc.play();
	}

}
