package spring_MVC;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Controlador {
	
	//encargado de mapear cual es el archivo que queremos mostrar
	@RequestMapping
	public String muestraPagina() {
		
		return "PaginaEjemplo";
	}
}
