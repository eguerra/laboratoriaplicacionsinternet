package spring_MVC;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/alumno")
public class AlumnoController {

	public AlumnoController() {
		// TODO Auto-generated constructor stub
	}
	
	@RequestMapping("/muestraFormulario")
	public String muestraformulario(Model modelo) {
		
		Alumno miAlumno = new Alumno();
		
		modelo.addAttribute("miAlumno", miAlumno);
		
		return "alumnoRegistroFormulario";
	}
	
	@RequestMapping("/procesaFormulario")
	public String procesarFormulario(@ModelAttribute("miAlumno") Alumno miAlumno) {
		
		return "confirmacionRegistroAlumno";
	}
	
	

}
