package spring_MVC;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/secundario") //deberemos a�adir "secundario" a la ruta para que se fije en este metodo
public class tirarControlador {

	@RequestMapping("/procesarFormulario2")
	public String procesarFormulario2(@RequestParam("NombreAlumno") String nombre, Model modelo) {
		
		nombre += " Es el peor alumno!!";
		
		String mensajeFinal = "�Qui�n es el peor? " + nombre;
		//agregar info al modelo
		modelo.addAttribute("ID_Mensaje", mensajeFinal)	;
		
		return "HolaAlumnosSpring";
	}

}
