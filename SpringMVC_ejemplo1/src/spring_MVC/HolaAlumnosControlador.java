package spring_MVC;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HolaAlumnosControlador {

	@RequestMapping("/muestraFormulario")
	public String muestraFormulario() {
		// Muestra el formulario
		
		return "HolaAlumnosFormulario";
	}
	
	@RequestMapping("/procesarFormulario")
	public String procesarFormulario() {
		
		return "HolaAlumnosSpring";
	}
	
	//@RequestMapping("/procesarFormulario2")
	//public String procesarFormulario2(HttpServletRequest request, Model modelo) {
	//String nombre = request.getParameter("NombreAlumno");
	
	@RequestMapping("/procesarFormulario2")
	public String procesarFormulario2(@RequestParam("NombreAlumno") String nombre, Model modelo) {
		
		nombre += " Es el mejor alumno!!";
		
		String mensajeFinal = "�Qui�n es el mejor? " + nombre;
		//agregar info al modelo
		modelo.addAttribute("ID_Mensaje", mensajeFinal)	;
		
		return "HolaAlumnosSpring";
	}

}
