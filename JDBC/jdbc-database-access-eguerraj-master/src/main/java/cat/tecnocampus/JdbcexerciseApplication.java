package cat.tecnocampus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdbcexerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdbcexerciseApplication.class, args);
	}
}
