package cat.tecnocampus;

public interface ClassroomDAO {
	
	public abstract void createClassroom(String classroomName);
	public abstract Classroom getClassroomByName(String classroomName);
	public abstract void deleteClassroomByName(String classroomName);
	public abstract void updateClassroomByName(String classroomName);
	
	//https://www.youtube.com/watch?v=4CxVy_1IhPg&list=PLzS3AYzXBoj8nQeedH6oAZiCuyEwhhzyL&index=2
}
