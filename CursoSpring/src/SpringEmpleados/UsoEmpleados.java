package SpringEmpleados;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoEmpleados {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*
		//Creacion de Objetos de tipo Empleado
		
			Empleados e1 = new DirectorEmpleado();
		
		
		//Uso de objetos creados
			
			System.out.println(e1.getTareas());
		*/
		
		// 1.Cargar archivo XML
		ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		/*
		// 2.Pedir el Bean
		Empleados Juan = contexto.getBean("miEmpleado", Empleados.class);
		
		// 3.Utilizar el Bean
		System.out.println(Juan.getTareas());
		System.out.println(Juan.getInforme());
		*/
		
		
		
		SecretarioEmpleado Maria = contexto.getBean("miSecretario", SecretarioEmpleado.class);
		JefeEmpleado Pablo = contexto.getBean("miEmpleado", JefeEmpleado.class);
		System.out.println(Maria.getTareas());
		System.out.println(Maria.getInforme());
		System.out.println("Email: "+Maria.getEmail());
		System.out.println("Empresa: "+Maria.getNombreEmpresa());
		
		System.out.println("Email: "+Pablo.getEmail());
		System.out.println("Empresa: "+Pablo.getNombreEmpresa());
		// 4.Cerrar el XML
		contexto.close();
		
	}

}
