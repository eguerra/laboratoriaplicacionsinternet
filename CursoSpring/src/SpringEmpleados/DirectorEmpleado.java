package SpringEmpleados;

public class DirectorEmpleado implements Empleados {

	//Creacion del campo de tipo CreacionInforme<interfaz>
	private CreacionInformes informeNuevo;
	
	//Creacion del constructor que inyecta la dependencia
	public DirectorEmpleado(CreacionInformes informeNuevo) {
		this.informeNuevo=informeNuevo;
	}
	
	public String getTareas() {
		// TODO Auto-generated method stub
		return "Tareas Director";
	}

	
	public String getInforme() {
		// TODO Auto-generated method stub
		return "InformeDirector: "+informeNuevo.getInforme();
	}


}
