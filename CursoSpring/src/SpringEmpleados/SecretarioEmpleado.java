package SpringEmpleados;

public class SecretarioEmpleado implements Empleados {
	
	private CreacionInformes informeNuevo;
	private String email;
	private String nombreEmpresa;
	
	
	//Encargado de crear la inyecci�n de dependencia
	public void setInformeNuevo(CreacionInformes informeNuevo) {
		this.informeNuevo = informeNuevo;
	}

	public String getTareas() {
		// TODO Auto-generated method stub
		return "Tareas de Secretario";
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	@Override
	public String getInforme() {
		// TODO Auto-generated method stub
		return "Informe de Secretario "+informeNuevo.getInforme();
	}

}
