package SpringAnotaciones;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("SpringAnotaciones")
//Escanea las clases que estan en este paquete en busca de anotaciones
@PropertySource("classpath:datosEmpresa")
public class Config {

	public Config() {
		// TODO Auto-generated constructor stub
	}
	
	//Definir bean para InformeFinanciero
	@Bean
	public InformeFinanciero informeFinanceiroCompras() {//Sera el ID del Bean inyectado
		
		return new InformeFinancieroCompras();
	}
	
	
	//definir bean para DirectorFinanciero e Inyectar dependencias
	@Bean
	public Empleados DirectorFinanciero() {
		
		return new DirectorFinanciero(informeFinanceiroCompras());
	}
}
