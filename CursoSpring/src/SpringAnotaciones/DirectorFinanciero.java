package SpringAnotaciones;

import org.springframework.beans.factory.annotation.Value;

public class DirectorFinanciero implements Empleados {

	public DirectorFinanciero(InformeFinanciero informeFinanciero) {
		super();
		this.informeFinanciero = informeFinanciero;
	}

	@Override
	public String getTareas() {
		// TODO Auto-generated method stub
		return "Gestion y Direccion";
	}

	@Override
	public String getInforme() {
		// TODO Auto-generated method stub
		return informeFinanciero.getInformeFinanciero();
	}
	
	
	public String getEmail() {
		return email;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}


	//crear campo encapsulado para la injecci�n de dependencia
	private InformeFinanciero informeFinanciero;
	
	@Value("${email}")
	private String email;
	
	@Value("${nombre}")
	private String nombreEmpresa;
}
