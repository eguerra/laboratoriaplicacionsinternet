package SpringAnotaciones;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoAnnotations {

	public static void main(String[] args) {
		
		//Leer el xml de configuracion
		ClassPathXmlApplicationContext con = new ClassPathXmlApplicationContext("applicationContextAnotaciones.xml");
		
		
		//pedir un bean al contenedor 
		Empleados e1 = con.getBean("comercialExperimentado",Empleados.class);
		
		
		//usar el bean
		System.out.println(e1.getTareas());
		System.out.println(e1.getInforme());
		
		
		//cerrar el contexto
		con.close();
		
	}

}
