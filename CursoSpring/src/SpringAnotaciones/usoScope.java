package SpringAnotaciones;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;


public class usoScope {

	public usoScope() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {


		//Leer el xml de configuracion
		//ClassPathXmlApplicationContext con = new ClassPathXmlApplicationContext("applicationContextAnotaciones.xml");
		
		
		//Leer la classe de configuración
		AnnotationConfigApplicationContext con = new AnnotationConfigApplicationContext(Config.class);
		
		//Pedir bean al contenedor
		Empleados e1 = con.getBean("DirectorFinanciero",Empleados.class);
		//System.out.println(e1.getInforme());
		//System.out.println(e1.getTareas());	
		
		DirectorFinanciero e2 =con.getBean("DirectorFinanciero",DirectorFinanciero.class);
		System.out.println(e2.getEmail());
		System.out.println(e2.getNombreEmpresa());	
		/*
		//Leer el xml de configuracion
		ClassPathXmlApplicationContext con = new ClassPathXmlApplicationContext("applicationContextAnotaciones.xml");
		
		
		//Pedir bean al contenedor
		Empleados e1 = con.getBean("comercialExperimentado",Empleados.class);
		Empleados e2 = con.getBean("comercialExperimentado",Empleados.class);
		
		//apuntan al mismo objeto en memoria
		if (e1 == e2) {
			System.out.println("Apuntan al mismo objeto en memoria");
			System.out.println(e1+"\n"+e2);
		}
		else {
			System.out.println("No apuntan al mismo objeto en memoria");
			System.out.println(e1+"\n"+e2);
		}
		*/
		
		//cerrar contexto
		con.close();
	}

}
