package SpringAnotaciones;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//Creamos nuestro bean
@Component
@Scope("singleton") 
/*  especificcamos que trabaje con el patron <Prototype> para	que sea capaz de crear
 * varias instancias pertenecientes a esta classe
 * */
public class ComercialExperimentado implements Empleados {
	
	
	//ejecucion despues de la creaci�n del bean
	@PostConstruct
	public void despuesCreaci�n() {
		System.out.println("Ejecutando despues de la creacion del Bean");
	}
	
	//ejecuci�n antes del cierre del contenedor Spring
	@PreDestroy
	public void despuesDestruccion() {
		System.out.println("Ejecutando antes de la destruccion");
	}
	
	
	/*
	 * Mira de todas las classes del paquete si alguna implementa la interficie 
	 * InformeFinanciero, de all� sacar� el informe que guardar� en la varialble
	 * nuevoInforme.
	 
	
	public ComercialExperimentado(InformeFinanciero nuevoInforme) {
		super();
		this.nuevoInforme = nuevoInforme;
	}
	*/ 
	
	public ComercialExperimentado() {
		
	}


	public void setNuevoInforme(InformeFinanciero nuevoInforme) {
		this.nuevoInforme = nuevoInforme;
	}

	@Autowired
	@Qualifier("informeFinancieroTrim2") // bean ID que debe utilizar
	private InformeFinanciero nuevoInforme;
	
	public String getTareas() {
		// TODO Auto-generated method stub
		return "Vender bien";
	}

	public String getInforme() {

		return nuevoInforme.getInformeFinanciero();
		//return "Informe generado por el comercialExperimentado";
	}

}


